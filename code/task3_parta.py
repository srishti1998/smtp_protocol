# importing necessary libraries

from email.message import Message
from email.mime import text
from email.mime.text import MIMEText
import smtplib # module for actual sending function
import optparse # library for parsing command line options


# creating mimetext which is a object that stores plain and HTML version of our messages
msg = MIMEText(    
    'Congratulations!!', 'plain')
msg['From'] = '###@gmail.com' # senders's email address
msg['To'] = '###@bgsu.edu'   # reciever's email address
msg['Subject'] = 'wohoo, you won a lucky draw!!'  # subject of the email
password= input('Please enter the password to send the message: ') # prompt to take passwors as input from sender

# converting msg into string
content = msg . as_string()

# connecting to mail server on which we wanna send msg using port 587 because TLS ecryption has default port 587
server = smtplib.SMTP("smtp.gmail.com", port=587)
print("connecting")

#  transport layer security
server.starttls() # starting an unsecured smtp conncetion and encrypting it using starttls
print("Session started")

# logging
server.login('shastri.srishti@gmail.com', password)
print("Logged in successfully")

# sendmail() method to send email
server.sendmail('###@gmail.com', '###@bgsu.edu', content)
print("Mail Sent")

# disconnecting the mail server by quiting.
server.quit()

# STEPS TO FOLLOW BEFORE RUNNING THIS CODE.

# 1. Before running this code you need to open your email account through web browser.
# 2. Then click on google account icon at top right corner
# 3. Click on Manage your google account
# 4. Click on security and go to less secure app access
# 5. Turn on allow less secure apps option
# 6. Now run this code





