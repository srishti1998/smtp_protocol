# importing necessary libraries
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders

# creating mimetext which is a object that stores plain and HTML version of our messages
body ='Congratulations!! Please refer the document for details.'
msg = MIMEMultipart()
msg['From'] = "###@gmail.com" # senders's email address
msg['To'] = '###@bgsu.edu'   # reciever's email address
msg['Subject'] = 'wohoo, you won a lucky draw!!'     # subject of the email
password= input('Please enter the password to send the message: ') # prompt to take passwors as input from sender
msg.attach(MIMEText(body, 'plain'))

pdfname = 'document.pdf' # name of the pdf you want to send
binary_pdf = open(pdfname, 'rb') #opening the file in binary
payload = MIMEBase('application', 'octate-stream', Name=pdfname)
payload.set_payload((binary_pdf).read())
encoders.encode_base64(payload) #encoding the binary into base64
payload.add_header('Content-Decomposition', 'attachment', filename=pdfname) #adding header to the pdf file
msg.attach(payload)

#default port number for TLS IS 587

server = smtplib.SMTP("smtp.gmail.com", port=587)
print("Connected To Mail Server.")

#  transport layer security
server.starttls()
print("Encrypted Session started")

# logging
server.login("shastri.srishti@gmail.com", password)
print("Logging Into Mail Server.")
text = msg.as_string()

# sendmail() method to send email
server.sendmail("###@gmail.com",'###@bgsu.edu' , text)
print("Sending")
print("Mail Sent")

# disconnecting the mail server by quiting.
server.quit()

# STEPS TO FOLLOW BEFORE RUNNING THIS CODE.

# 1. Before running this code you need to open your email account through web browser.
# 2. Then click on google account icon at top right corner
# 3. Click on Manage your google account
# 4. Click on security and go to less secure app access
# 5. Turn on allow less secure apps option
# 6. Now run this code
